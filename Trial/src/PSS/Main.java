package PSS;

import java.io.File; // Import the File class
import java.io.FileNotFoundException; // Import this class to handle errors
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public static boolean finish;

	public static boolean ReadFile() {
		boolean run = false;
		try {
			File myObj = new File("boot.ini");
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				String Processor = myReader.nextLine();
				String CPUSpeed = myReader.nextLine();

				String DiskSize = myReader.nextLine();
				String Memory = myReader.nextLine();

				String Peripherals = myReader.nextLine();
				Pattern p = Pattern.compile("(\\d+(?:\\.\\d+))");
				Matcher m = p.matcher(CPUSpeed);
				Matcher m1 = p.matcher(Memory);
				while (m.find() && m1.find()) {
					double CPUSpeed1 = Double.parseDouble(m.group(1));
					double Memory1 = Double.parseDouble(m1.group(1));

					if (Memory1 > 0.16 && CPUSpeed1 > 0.2) {
						System.out.println("System requirements have been met\n");
						run = true;

					} else {
						run = false;

					}

				}

			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

		return run;
	}

	public static void Timer(long t, boolean c, String Pattern) {
		try {
			for (int i = 0; i < 20; i++) {
				Thread.sleep(t);
				if (c == true) {
					System.out.print(Pattern);
				}
			}

		} catch (InterruptedException e) {
			System.out.println("Error Detected");
		}
	}

	@SuppressWarnings({ "resource" })
	public static void fcfs() throws InterruptedException, NumberFormatException, IOException {

		System.out.print("-----------------------   FCFS  --------------------------");

		Scanner input = new Scanner(System.in);
		finish = false;

		System.out.println("\nEnter Process name, CPU Burst Time and Arrival time\n ");
		int x = 10;

		long startTime = System.currentTimeMillis();

		while (!finish && (System.currentTimeMillis() - startTime) < x * 1000) {

			Process pro = new Process();

			String pn = "";
			String bt = "";
			String at = "";

			pn = input.next();
			if (pn.equalsIgnoreCase("exit")) {
				finish = true;

				System.out.println("\nKhalas\n");

				break;

			}

			else {
				bt = String.valueOf(input.nextFloat());
				at = String.valueOf(input.nextFloat());

			}

			System.out.println("\nEnter Process name, CPU Burst Time and Arrival time\n ");

			pro.process_name = pn;
			pro.burstTime = Float.parseFloat(bt);
			pro.arrivalTime = Float.parseFloat(at);
			Shared.getProcessScheduler().add(pro);

			if ((System.currentTimeMillis() - startTime) > x * 1000) {
				System.out.println("\nKhalas\n");
				break;

			}

		}

	}

	public static void execute() {

		{
			Collections.sort(Shared.getProcessScheduler(), new comp());
		}

		System.out.print("Processing ");
		Timer(50, true, ".");
		System.out.println("\n");
		result(Shared.getProcessScheduler(), Shared.getProcessScheduler().size(), false);

	}

	public static void selectOption() throws InterruptedException {
		Scanner input = new Scanner(System.in);
		Semaphore sem = new Semaphore(1);

		while (true) {

			Method method = new Method();

			ThreadA mA = new ThreadA("Thread A", method, sem);
			ThreadB mB = new ThreadB("Thread B", method, sem);
			ThreadB mC = new ThreadB("Thread C", method, sem);

			mA.start();
			mA.join();

			if (Main.finish == true) {
				mC.start();
				mC.join();
				System.out.print("Programme is shutting down ");
				Timer(50, true, ".");
				System.exit(0);
			}

			mB.start();
			mB.join();

		}
	}

	public static void result(ArrayList<Process> ProcessScheduler, int n, boolean pre) {

		float completion_time[] = new float[n];
		float turn_around_time[] = new float[n];
		float waiting_time[] = new float[n];

		Iterator<Process> it = ProcessScheduler.iterator();

		float sumer = 0;
		int counter = 0;

		int total_waiting = 0, total_turn_around = 0, c = -8;
		if (finish == true) {
			System.out.println("\n                          Final Table : \n");
		}

		else {
			System.out.println("\n                           Table : \n");

		}
		System.out.println(
				"Procs Exec order   Arrival TIme    Burst Time      Completion Time    Turn Around Time     Waiting Time");

		while (it.hasNext()) {
			Timer(50, false, "");
			Process pro = new Process();
			pro = it.next();
			if (c == -8) {
				sumer = pro.arrivalTime;
				c = 0;
			}
			sumer += pro.burstTime;

			if (pre == false) {
				completion_time[counter] = sumer;

			} else {
				completion_time[counter] = pro.compTime;
			}

			turn_around_time[counter] = completion_time[counter] - pro.arrivalTime;
			waiting_time[counter] = turn_around_time[counter] - pro.burstTime;
			total_waiting += waiting_time[counter];
			total_turn_around += turn_around_time[counter];
			System.out.print("\t  " + pro.process_name + "\t\t" + pro.arrivalTime + "\t\t" + pro.burstTime + "\t\t"
					+ completion_time[counter]);
			System.out.println("\t\t" + turn_around_time[counter] + "\t\t\t" + waiting_time[counter]);
			++counter;
		}

		System.out.println("\nResult :");
		System.out.println("Average Waiting Time     = " + (float) total_waiting / n);
		System.out.println("Average Turn Around Time = " + (float) total_turn_around / n + "\n");
	}

	public static void main(String ar[]) throws InterruptedException {
		if (ReadFile() == true) {
			selectOption();
		} else {
			System.out.println("Minimum requirements could not be met");

		}

	}
}
