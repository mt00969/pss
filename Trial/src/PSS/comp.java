package PSS;

import java.util.Comparator;

public class comp implements Comparator<Process> {
	@Override
	public int compare(Process arg0, Process arg1) {
		if (arg0.arrivalTime > arg1.arrivalTime) {
			return 1;
		} else if (arg0.arrivalTime < arg1.arrivalTime) {
			return -1;
		}
		return 0;
	}
}
