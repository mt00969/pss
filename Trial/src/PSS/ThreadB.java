package PSS;

import java.util.concurrent.Semaphore;

public class ThreadB extends Thread {

	String threadName;
	Method mB;
	Semaphore sem;

	ThreadB(String threadName, Method m, Semaphore sem) {
		this.threadName = threadName;
		mB = m;
		this.sem = sem;
	}

	@Override
	public void run() {

		synchronized (mB) {
			mB.print(1, threadName);

			try {
				System.out.print(threadName + " is waiting for a permit ");

				Main.Timer(50, true, ".");
				System.out.println("\n");
				sem.acquire();
				System.out.print(threadName + " gets the permit ");

				Main.Timer(50, true, ".");
				System.out.println("\n");
				Main.execute();

				System.out.print(threadName + " releases the permit ");

				Main.Timer(50, true, ".");
				System.out.println("\n");
				sem.release();

			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}

	}

}
