
package DiskScheduling;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

public class SplashScreen extends JWindow {

	private int duration;

	public SplashScreen(int d) {
		duration = d;
	}

	public void showSplash() {
		JPanel content = (JPanel) getContentPane();
		content.setBackground(Color.white);

		int width = 450;
		int height = 115;
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screen.width - width) / 2;
		int y = (screen.height - height) / 2;
		setBounds(x, y, width, height);

		JLabel copyrt = new JLabel("Disk Scheduling", JLabel.CENTER);
		copyrt.setFont(new Font("Times New Roman", Font.BOLD, 40));
		JLabel copyrt2 = new JLabel("A Project in Operating System", JLabel.CENTER);
		copyrt2.setFont(new Font("Times New Roman", Font.BOLD, 15));
		content.add(copyrt, BorderLayout.CENTER);
		content.add(copyrt2, BorderLayout.SOUTH);
		Color oraRed = new Color(0, 51, 51);
		content.setBorder(BorderFactory.createLineBorder(oraRed, 10));

		setVisible(true);

		try {
			Thread.sleep(duration);
		} catch (Exception e) {
		}

	}

	public void showSplashAndExit() {
		showSplash();
		dispose();
	}
}
